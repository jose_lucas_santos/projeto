function validaLogin() {
    let userTxt = localStorage.getItem("userLogged");
    
    if (!userTxt) {
        window.location="index.html";    
    }

}

function logout() {
    localStorage.removeItem("userLogged");
    window.location="index.html"; 
}

function voltar() {
    window.location = "dashmenu.html";
}

function gerarRelatorioAlarmes() {
    event.preventDefault();   // evita o comportamento padrão do form. não envia os dados do form.

    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;

    let dataMsg = {
        dt1:dataini,
        dt2:datafim
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(dataMsg),
        headers:{
            'Content-type':'application/json'
        }
    }

    fetch("http://localhost:8080/evento/alarmes",cabecalho) 
        .then(res => res.json() )    // extrai os dados do retorno
        .then(result => preencheTotalAlarmes(result));
}

function preencheTotalAlarmes(result) {
    console.log(result);
    if (result.length == 0) {
        alert("Nenhum registro selecionado!");
        /*document.getElementById("msgError").innerHTML = "Nenhum registro selecionado";*/
        document.getElementById("tabela").innerHTML = null;
    } else {    
    console.log(result);
    let tabela = `<table  class="table table-sm">
                    <thead class="thead-dark"> 
                        <tr> 
                            <th>Alarme</th> 
                            <th>Quantidade no periodo</th>
                        </tr>
                      </thead>
                    <tbody>`;

    for (let i = 0; i < result.length; i++) {
       tabela = tabela + `<tr> 
            <td>${result[i][0]} </td> 
            <td>${result[i][1]} </td>
                          </tr>`          
    }

    tabela = tabela + `</tbody>
                    </table>`;
    document.getElementById("tabela").innerHTML = tabela;
    }
}
